﻿using System;

namespace ObjectPool
{
  class ExampleObject
  {
    public int[] Nums { get; set; }

    // Это медленный конструктор
    public ExampleObject()
    {
      Nums = new int[1000000];
      var rand = new Random();
      for (int i = 0; i < Nums.Length; i++)
      {
        Nums[i] = rand.Next();
      }
    }

    public double GetValue(long i) => Math.Sqrt(Nums[i]);
  }
}
