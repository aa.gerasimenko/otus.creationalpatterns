﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace ObjectPool
{
  class Program
  {
    static void Main(string[] args)
    {
      using var cts = new CancellationTokenSource();

      _ = Task.Run(() =>
      {
        if (char.ToUpperInvariant(Console.ReadKey().KeyChar) == 'C')
        {
          cts.Cancel();
        }
      });

      var pool = new ObjectPool<ExampleObject>(() => new ExampleObject());

      var watch = Stopwatch.StartNew();

      Parallel.For(0, 1000, (i, loopState) =>
      {
        var example = pool.Get();
        try
        {
          Console.CursorLeft = 0;
          Console.WriteLine($"{example.GetValue(i):####.####}");
        }
        finally
        {
          pool.Return(example);
        }

        if (cts.Token.IsCancellationRequested)
        {
          loopState.Stop();
        }
      });

      watch.Stop();
      Console.WriteLine($"Completed for {watch.Elapsed}.");
      Console.ReadLine();
    }
  }
}
