﻿using System.Text.Json;

namespace Factory.Domain
{
  class Money
  {
    public Money(decimal amount, string currency)
    {
      Amount = amount;
      Currency = currency;
    }

    public decimal Amount { get; }
    public string Currency { get; }

    public override string ToString()
    {
      return JsonSerializer.Serialize(this);
    }
  }
}
