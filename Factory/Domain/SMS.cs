﻿using System.Text.Json;

namespace Factory.Domain
{
  class SMS: INotification
  {
    public SMS(string message, string receiver)
    {
      Message = message;
      Receiver = receiver;
    }

    public string Message { get; }
    public string Receiver { get; }

    public override string ToString()
    {
      return JsonSerializer.Serialize(this);
    }
  }

  class Email : INotification
  {
    public Email(string message, string receiver)
    {
      Message = message;
      Receiver = receiver;
    }

    public string Message { get; }
    public string Receiver { get; }

    public override string ToString()
    {
      return JsonSerializer.Serialize(this);
    }
  }

  public interface INotification
  {
    public string Message { get; }
    public string Receiver { get; }
  }
}
