﻿using Factory.Domain;
using System;

namespace Factory.Storage
{
  public class TwoFAStorage
  {
    public void Save(INotification message)
    {
      Console.WriteLine($"Сохранили в базу {message}");
    }
  }
}