﻿using Factory.Domain;
using Factory.Storage;
using System;

namespace Factory.Service
{
    interface INotificationGenerator
    {
        INotification CreateNotification(string message, string receiver);
        string GenerateCode();
    }

    class SmsNotificationGenerator : INotificationGenerator
    {
        public INotification CreateNotification(string message, string receiver)
        {
            return new SMS(message, receiver);
        }

        public string GenerateCode()
        {
            return "SMS-123456";
        }
    }

    class EmailNotificationGenerator : INotificationGenerator
    {
        public INotification CreateNotification(string message, string receiver)
        {
            return new Email(message, receiver);
        }

        public string GenerateCode()
        {
            return "Email-123456123456123456123456123456";
        }
    }


    internal class TwoFAService
    {
        private readonly TwoFAStorage _2faStorage;
        private readonly INotificationGenerator _notificationGenerator;

        public TwoFAService(TwoFAStorage twoFaStorage, INotificationGenerator notificationGenerator)
        {
            _2faStorage = twoFaStorage;
            _notificationGenerator = notificationGenerator;
        }

        public void Run()
        {
            var message = SendConfirmationSMS();
            _2faStorage.Save(message);
        }

        private INotification SendConfirmationSMS()
        {
            var code = _notificationGenerator.GenerateCode();
            var receiver = "712300000000";
            var notification = _notificationGenerator.CreateNotification($"Confirm the withdrawal with code: {code}", receiver);
            Console.WriteLine($"Сообщение отправлено: {notification}");
            return notification;
        }
    }
}