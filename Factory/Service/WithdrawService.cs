﻿using Factory.Domain;
using System;

namespace Factory.Service
{
  internal class WithdrawService
  {
    private readonly BalanceProvider _balanceProvider;
    private readonly TwoFAService _2faService;

    public WithdrawService(BalanceProvider balanceProvider, TwoFAService twoFAService)
    {
      _balanceProvider = balanceProvider;
      _2faService = twoFAService;
    }

    public void StartWithdraw(Money money)
    {
      CheckEnoughBalance(money);
      _2faService.Run();
    }

    private void CheckEnoughBalance(Money money)
    {
      var balance = _balanceProvider.GetBalance(money.Currency);
      if (balance < money.Amount)
      {
        throw new ApplicationException("Not enough balance");
      }
    }
  }
}