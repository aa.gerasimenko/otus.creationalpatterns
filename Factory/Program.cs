﻿using Factory.Domain;
using Factory.Service;
using Factory.Storage;
using System;

namespace Factory
{
  internal class Program
  {
    private static void Main(string[] args)
    {
      var balanceProvider = new BalanceProvider();
      var two2FaStorage = new TwoFAStorage();
      var two2FaService = new TwoFAService(two2FaStorage, new EmailNotificationGenerator());

      var withdrawService = new WithdrawService(balanceProvider, two2FaService);

      withdrawService.StartWithdraw(new Money(435, "USD"));

      Console.ReadKey();
    }
  }
}