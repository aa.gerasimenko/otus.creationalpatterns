﻿using PatternsCreational.Repository;
using PatternsCreational.Repository.Model;
using System;
using System.Collections.Generic;

namespace PatternsCreational.Service
{
    class CarFleetPrinter : IDisposable
    {
        private readonly ICarRepository _carRepository;

        public CarFleetPrinter(ICarRepository carRepository)
        {
            _carRepository = carRepository;
            _carRepository.FleetUpdated += OnFleetUpdated;
        }

        public void Dispose()
        {
            _carRepository.FleetUpdated -= OnFleetUpdated;
        }

        private void OnFleetUpdated(object sender, EventArgs e)
        {
            List<CarEntity> fleet = _carRepository.GetAll();
            Console.WriteLine($"Новый автопарк: ${string.Join(", ", fleet)}");
        }
    }
}
