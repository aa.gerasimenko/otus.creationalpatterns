﻿using PatternsCreational.Repository.Model;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;

namespace PatternsCreational.Repository
{
    class CarRepository : ICarRepository
    {
        static CarRepository _instance;
        static object _sync = new object();

        private CarRepository()
        {
            Console.WriteLine("Создали CarRepository\n");
        }

        public static CarRepository Instance
        {
            get
            {
                if (_instance != null)
                    return _instance;

                lock (_sync)
                {
                    if (_instance == null)
                        _instance = new CarRepository();

                    return _instance;
                }
            }
        }

        private readonly ConcurrentDictionary<Guid, CarEntity> _store = new ConcurrentDictionary<Guid, CarEntity>();

        public event EventHandler FleetUpdated;

        public void Add(string manufacturer, string model)
        {
            var id = Guid.NewGuid();
            var car = new CarEntity(id, manufacturer, model);
            _store[id] = car;

            Console.WriteLine($"Добавили автомобиль {id}\n");

            FleetUpdated?.Invoke(this, new EventArgs());
        }

        public List<CarEntity> GetAll()
        {
            return _store.Values.ToList();
        }
    }
}
