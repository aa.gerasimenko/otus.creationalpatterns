﻿using PatternsCreational.Repository.Model;
using System;
using System.Collections.Generic;

namespace PatternsCreational.Repository
{
    interface ICarRepository
    {
        event EventHandler FleetUpdated;

        void Add(string manufacturer, string model);
        List<CarEntity> GetAll();
    }
}