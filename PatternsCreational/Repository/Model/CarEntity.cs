﻿using System;
using System.Text.Json;

namespace PatternsCreational.Repository.Model
{
    class CarEntity
    {
        public Guid Id { get; }
        public string Manufacturer { get; }
        public string Model { get; }

        public CarEntity(Guid id, string manufacturer, string model)
        {
            Id = id;
            Manufacturer = manufacturer;
            Model = model;
        }

        public override string ToString()
        {
            return JsonSerializer.Serialize(this);
        }
    }
}
