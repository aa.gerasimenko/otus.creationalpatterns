﻿namespace PatternsCreational.Controller.Model
{
    class AddCarRequest
    {
        public string Manufacturer { get; }
        public string Model { get; }

        public AddCarRequest(string manufacturer, string model)
        {
            Manufacturer = manufacturer;
            Model = model;
        }
    }
}
