﻿using PatternsCreational.Controller.Model;
using PatternsCreational.Repository;

namespace PatternsCreational.Controller
{
    class RestController
    {
        private readonly ICarRepository _carRepository;

        public RestController(ICarRepository carRepository)
        {
            _carRepository = carRepository;
        }

        public void AddCar(AddCarRequest request)
        {
            _carRepository.Add(request.Manufacturer, request.Model);
        }
    }
}
