﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.Json;

namespace PatternsCreational._Practice
{    
    class HtmlProvider
    {
        private readonly Dictionary<string, string> _cache = new Dictionary<string, string>();

        public string GetHtml(string url)
        {
            if (!_cache.TryGetValue(url, out string html))
            {
                var request = WebRequest.Create(url);
                using (var response = (HttpWebResponse)request.GetResponse())
                using (var stream = response.GetResponseStream())
                using (var reader = new StreamReader(stream))
                {
                    Console.WriteLine($"Reading html at {url}");
                    html = reader.ReadToEnd();
                }
                _cache[url] = html;                
            }
            return html;
        }
    }

    class User
    {
        private int _userId;
        private string _firstName;
        private string _lastName;
        private int _age;
        private string _hobby;
        private int _height;
        private int _weight;
        private IEnumerable<User> _children;
        private string _city;
        private string _parameters;

        public int UserId => _userId;
        public string FirstName => _firstName;
        public string LastName => _lastName;
        public int Age => _age;
        public string Hobby => _hobby;
        public int Height => _height;
        public int Weight => _weight;
        public IEnumerable<User> Children => _children;
        public string City => _city;
        public string Parameters => _parameters;

        public User(int userId, string firstName, string lastName, int age, string hobby, int height, int weight, IEnumerable<User> children, string city, string parameters)
        {
            _userId = userId;
            _firstName = firstName;
            _lastName = lastName;
            _age = age;
            _hobby = hobby;
            _height = height;
            _weight = weight;
            _children = children;
            _city = city;
            _parameters = parameters;
        }

        public override string ToString()
        {
            return JsonSerializer.Serialize(this);
        }
    }

    static class TestPractice
    {
        static public void Run()
        {
            var user1 = new User(
                    userId: 1,
                    firstName: "Ivan",
                    lastName: "Petrov",
                    age: 35,
                    hobby: "Fishing",
                    height: 175,
                    weight: 70,
                    children: Enumerable.Empty<User>(),
                    city: "Moscow",
                    parameters: new HtmlProvider().GetHtml("https://jsonplaceholder.typicode.com/todos/35"));

            var user2 = new User(
                userId: 2,
                firstName: "Sergei",
                lastName: "Pugachov",
                age: 29,
                hobby: "Football",
                height: 185,
                weight: 90,
                children: Enumerable.Empty<User>(),
                city: "Moscow",
                parameters: new HtmlProvider().GetHtml("https://jsonplaceholder.typicode.com/todos/29"));

            var user3 = new User(
                userId: 3,
                firstName: "Alexei",
                lastName: "Konstantinov",
                age: 35,
                hobby: "Chess",
                height: 182,
                weight: 75,
                children: Enumerable.Empty<User>(),
                city: "Moscow",
                parameters: new HtmlProvider().GetHtml("https://jsonplaceholder.typicode.com/todos/35"));

            Console.WriteLine($"\n\nПользователи:\n\n{user1}\n\n{user2}\n\n{user3}");
        }
    }
}
