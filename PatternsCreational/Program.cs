﻿
using PatternsCreational._Practice;
using PatternsCreational.Controller;
using PatternsCreational.Controller.Model;
using PatternsCreational.Domain;
using PatternsCreational.Repository;
using PatternsCreational.Service;

namespace PatternsCreational
{
  class Program
  {
    static void Main(string[] args)
    {
      Console.Clear();
      // Singleton:
      // Singleton();

      // Prototype:
      // Prototype();

      // Builder:
      Builder();

      // Practice:
      // TestPractice.Run();

      // Console.WriteLine("\nРабота программы завершена. Нажмите кнопку для выхода.");
      // Console.ReadKey();
    }

    static void Singleton()
    {
      // var carRepository = new CarRepository(); // TODO

      // Создаем автоматический принтер автопарка
      using var carFleetPrinter = new CarFleetPrinter(CarRepository.Instance);

      // создаем REST-контроллер
      var restController = new RestController(CarRepository.Instance);

      // добавляем автомобиль в автопарк            
      var addCarRequest = new AddCarRequest(manufacturer: "Mercedes", model: "c180");
      restController.AddCar(addCarRequest);
    }

    //static void MultiThreadSingleton()
    //{
    //    const int tasksCount = 10;
    //    using var barrierStart = new Barrier(tasksCount);
    //    using CountdownEvent waitingCompletionCountdown = new CountdownEvent(tasksCount);
    //    for (var i = 0; i < tasksCount; i++)
    //    {
    //        Task.Factory.StartNew(() =>
    //        {
    //            barrierStart.SignalAndWait();
    //            var carRepository = CarRepository.Instance;
    //            var fleet = carRepository.GetAll();
    //            waitingCompletionCountdown.Signal();
    //        });
    //    }
    //    waitingCompletionCountdown.Wait();
    //}

    static void Prototype()
    {
      var car1 = new Car(
          Guid.NewGuid(),
          "Mercedes",
          "c180",
          trailer: null);
      var car2 = new Car(
          Guid.NewGuid(),
          "Audi",
          "q5",
          trailer: new Car(Guid.NewGuid(), "Aurora", model: null, trailer: null));
      var car3 = new Car(
          Guid.NewGuid(),
          "Audi",
          "q5",
          trailer: new Car(Guid.NewGuid(), "Aurora", model: null, trailer: null),
          topTrunk: new TopTrunk(Guid.NewGuid(), "TRZ", "A10"));

      Console.WriteLine($"Авто 1: \n{car1}\n");
      Console.WriteLine($"Авто 2: \n{car2}\n");
      Console.WriteLine($"Авто 3: \n{car3}\n\n");

      var copyCar1 = car1.Copy();
      var copyCar2 = car2.Copy();
      var copyCar3 = car3.Copy();

      Console.WriteLine($"Копия авто 1: \n{copyCar1}\n");
      Console.WriteLine($"Копия авто 2: \n{copyCar2}\n");
      Console.WriteLine($"Копия авто 3: \n{copyCar3}\n");
    }

    static void Builder()
    {
      var car1 = new Car(
          Guid.NewGuid(),
          "Mercedes",
          "c180",
          trailer: null);
      var car2 = new Car(
          Guid.NewGuid(),
          "Audi",
          "q5",
          trailer: new Car(Guid.NewGuid(), "Aurora", model: null, trailer: null));
      /*var car3 = new Car(
          Guid.NewGuid(),
          "Audi",
          "q5",
          trailer: new Car(Guid.NewGuid(), "Aurora", model: null, trailer: null),
          topTrunk: new TopTrunk(Guid.NewGuid(), "TRZ", "A10"));*/

      var builder = new Car.Builder(Guid.NewGuid(), "Audi", "q5")
        .SetTrailer(new Car(Guid.NewGuid(), "Aurora", model: null, trailer: null))
        .SetTopTrunk(new TopTrunk(Guid.NewGuid(), "TRZ", "A10"));
      var car3  = builder.Build();

      Console.WriteLine($"Авто 1: \n{car1}\n");
      Console.WriteLine($"Авто 2: \n{car2}\n");
      Console.WriteLine($"Авто 3: \n{car3}\n");
    }
  }
}
