﻿using PatternsCreational.Common;
using System;
using System.Text.Json;

namespace PatternsCreational.Domain
{
    class TopTrunk: IPrototype<TopTrunk>
    {
        public Guid Id { get; }
        public string Manufacturer { get; }
        public string Model { get; }

        public TopTrunk(Guid id, string manufacturer, string model)
        {
            Id = id;
            Manufacturer = manufacturer;
            Model = model;
        }

        public override string ToString()
        {
            return JsonSerializer.Serialize(this);
        }

        public TopTrunk Copy()
        {
            return new TopTrunk(Id, Manufacturer, Model);            
        }
    }
}
