﻿using PatternsCreational.Common;
using System;
using System.Text.Json;

namespace PatternsCreational.Domain
{
    class Car : IPrototype<Car>
    {
        private Guid _id;
        private string _manufacturer;
        private string _model;
        private Car _trailer;
        private TopTrunk _topTrunk;
        private bool _isSportCar;

        public Guid Id => _id;
        public string Manufacturer => _manufacturer;
        public string Model => _model;
        public Car Trailer => _trailer;
        public TopTrunk TopTrunk => _topTrunk;
        public bool IsSportCar => _isSportCar;

        public Car(Guid id, string manufacturer, string model, bool isSportCar)
        {
            _id = id;
            _manufacturer = manufacturer;
            _model = model;
            _isSportCar = isSportCar;
        }

        public Car(Guid id, string manufacturer, string model, Car trailer)
        {
            _id = id;
            _manufacturer = manufacturer;
            _model = model;
            _trailer = trailer;
            _isSportCar = false;
        }

        public Car(Guid id, string manufacturer, string model, Car trailer, TopTrunk topTrunk)
        {
            _id = id;
            _manufacturer = manufacturer;
            _model = model;
            _trailer = trailer;
            _topTrunk = topTrunk;
            _isSportCar = false;
        }

        private Car(Guid id, string manufacturer, string model, bool isSportCar, Car trailer, TopTrunk topTrunk)
        {
            _id = id;
            _manufacturer = manufacturer;
            _model = model;
            _trailer = trailer;
            _topTrunk = topTrunk;
            _isSportCar = isSportCar;
        }


        public override string ToString()
        {
            return JsonSerializer.Serialize(this);
        }

        public Car Copy()
        {
            if (_isSportCar)
                return new Car(Id, Manufacturer, Model, true);

            var copyTrailer = Trailer?.Copy();
            var copyTopTrunk = TopTrunk?.Copy();

            return new Car(Id, Manufacturer, Model, copyTrailer, copyTopTrunk);
        }

        public class Builder
        {
            private Guid _id;
            private string _manufacturer;
            private string _model;
            private Car _trailer;
            private TopTrunk _topTrunk;
            private bool _isSportCar;

            // public void SetId(Guid id)
            // {
            //     _id = id;
            // }

            /*....*/
            public Builder(Guid id, string manufacturer, string model, bool isSportCar = false)
            {
                _id = id;
                _manufacturer = manufacturer;
                _model = model;
                _isSportCar = isSportCar;
            }

            public Builder SetTrailer(Car trailer)
            {
                if (!_isSportCar)
                    _trailer = trailer;
                return this;
            }
            public Builder SetTopTrunk(TopTrunk trunk)
            {
                if (!_isSportCar)
                    _topTrunk = trunk;
                return this;
            }

            public Car Build()
            {
                return new Car(_id, _manufacturer, _model, _isSportCar, _trailer, _topTrunk);
            }
        }
    }
}
