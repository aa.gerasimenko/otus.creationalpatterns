﻿namespace PatternsCreational.Common
{
    interface IPrototype<T>
    {
        T Copy();
    }
}
